# -*- coding: utf-8 -*-
"""
Created on Wed Oct  9 18:38:01 2019

@author: knovich
"""

#%%
import calculation as ca
import structure as st
test_per=[(0.5,2,1),(0.5,8,0),(0.5,2,1)]
struct=st.Structure(test_per,field=0.01,discr=30,last=True,repeat=200)
c=ca.Contour((-1.5,1),(-0.01,-1e-20),1000,1000)
func=lambda x:struct.eqn_stat(x,threads=1)
%time ca.circumvent(func,c)


#%%
test2=[(0.5,2,1),(0.5,8,0),(0.5,2,1),(0.5,8,0),(0.5,2,1)]
%time first_try(test2,50,0.01,(-0.001,-1e-6))