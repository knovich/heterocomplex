    module test

    use,intrinsic:: iso_c_binding
    implicit none
    
    integer,parameter::pr=c_long_double
    integer,parameter::pc=c_long_double_complex
    private
    public::by_array,by_one
    
    contains
    
    subroutine by_array(pts,f,n,num) bind(c)
    !DEC$ ATTRIBUTES DLLEXPORT::by_array
    integer(c_int),intent(in)           ::  n,num
    complex(pc),intent(in)              ::  pts(n)
    complex(pc),intent(inout)           ::  f(n)
    integer(c_int)                      ::  i
    
    !$OMP PARALLEL NUM_THREADS(num),SHARED(f,pts)
    !$OMP DO SCHEDULE(STATIC)
    do i=1,n
        f(i)=func(pts(i))
    end do
    !$OMP END DO NOWAIT
    !$OMP END PARALLEL
    
    end subroutine by_array
    
    
    subroutine by_array_single(pts,f,n) bind(c)
    !DEC$ ATTRIBUTES DLLEXPORT::by_array_single
    integer(c_int),intent(in)           ::  n
    complex(pc),intent(in)              ::  pts(n)
    complex(pc),intent(inout)           ::  f(n)
    integer(c_int)                      ::  i
    
    do i=1,n
        f(i)=func(pts(i))
    end do
    
    end subroutine by_array_single
    
    
    complex(pc) function by_one(arg) bind(c)
    !DEC$ ATTRIBUTES DLLEXPORT::by_one
    complex(pc),intent(in)              ::  arg
    complex(pc)                         ::  temp
    
    temp=func(arg)
    by_one=temp
    
    end function by_one
    
    
    !DEC$ ATTRIBUTES FORCEINLINE :: func
    complex(pc) function func(arg)
    complex(pc),intent(in)::arg
    complex(pc)::tm,res
    integer::i
    
    res=1
    do i=1,40000
        tm=exp((1._pr,1._pr)*sqrt(-arg+cmplx(5._pr*i,2._pr/i)))
        res=res*tm
    end do
    func=res
    
    end function func
    
    end module test