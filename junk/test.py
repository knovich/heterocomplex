# -*- coding: utf-8 -*-
"""
Created on Sat Jun 22 09:31:10 2019

@author: knovich
"""

import ctypes as ct
import numpy as np
import cProfile
class c_longcomplex(ct.Structure):
    _fields_ = ("re", ct.c_longdouble), ("im", ct.c_longdouble)
    
complex_p=ct.POINTER(c_longcomplex)
    
def f90_cmplx(z=0+0j):
    return complex_p(c_longcomplex(z.real,z.imag))

def py_cmplx(pz):
    return np.longcomplex(pz.contents.re+1j*pz.contents.im)
    
def load():
    global _lib
    _lib=ct.CDLL('test.dll')
    _lib.by_one.restype=c_longcomplex
    
def by_array(arr,threads=1):
    res=np.ndarray((arr.size,),dtype=np.longcomplex)
    _lib.by_array(arr.ctypes.data_as(ct.POINTER(ct.c_longdouble)),
                  res.ctypes.data_as(ct.POINTER(ct.c_longdouble)),
                  ct.byref(ct.c_int(arr.size)),
                  ct.byref(ct.c_int(threads)))
    return res

def by_array_s(arr):
    res=np.ndarray((arr.size,),dtype=np.longcomplex)
    _lib.by_array_single(arr.ctypes.data_as(ct.POINTER(ct.c_longdouble)),
                  res.ctypes.data_as(ct.POINTER(ct.c_longdouble)),
                  ct.byref(ct.c_int(arr.size)))
    return res
    
def by_one(en):
    res=_lib.by_one(f90_cmplx(en))
    return np.longcomplex(res.re+1j*res.im)

def prepare():
    return (np.linspace(-10,10,10000,dtype=np.longdouble)+
            1j*np.linspace(0.5,-0.5,10000,dtype=np.longdouble))
    
def test_one(arg):
    res=np.fromiter((by_one(x) for x in np.nditer(arg)),dtype=np.longcomplex)
    return res

def test_arr(arg):
    res=by_array(arg)
    return res

def release_dll():
    global _lib
    ct.windll.kernel32.FreeLibrary.argtypes = [ct.wintypes.HMODULE]
    h=_lib._handle
    del _lib
    ct.windll.kernel32.FreeLibrary(h)
    return

load()
x=prepare()
