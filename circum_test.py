# -*- coding: utf-8 -*-
"""
Created on Sat Nov  2 17:32:10 2019

@author: knovich
"""
#%%
import matplotlib.colors as clr
import matplotlib.pyplot as plt
%matplotlib qt

#%%
period=[(0.5,2,1),(0.5,8,0),(0.5,2,1.1),(0.5,8,0),(0.5,2,1)]
test=st.Structure(period,field=0.01,discr=30,last=True,repeat=200)
f=lambda x:np.angle(test.eqn_stat(x))
c2=ca.RectPath(-7-0.001j,-6-0.000000000005j,epsilon=0.1,Nroots=10,aspect=10)
#print(c2.walk_path(f))

#%%
r3,d3=ca.all_roots(c2,f,1e-15)
#%%
d4=[]
for x in d3:
    x.epsilon=1
    a,b=ca.all_roots(x,f,1e-15)
    r3+=a
    d4+=b

#%%
p=d[0]
while p:
    plt.plot(p.path.real,p.path.imag)
    p=p.parent

#%%
period=[(0.5,2,1),(0.5,8,0),(0.5,2,1)]
test=st.Structure(period,field=0.01,discr=30,last=True,repeat=50)
f=lambda x:np.angle(test.eqn_stat(x))
c1=ca.RectPath(-2.6-0.0014j,-2.67-0.0008j,epsilon=0.1,Nroots=1,aspect=1)
print(c1.walk_path(f))
#%%
r,d=ca.all_roots(c1,f,1e-15)

#%%
for x in c1.children:
    plt.figure()
    diffs=np.roll(x.path,-1)-x.path
    plen=np.cumsum(abs(diffs))
    plt.plot(plen,x.values)
    plt.scatter(plen,x.values,s=3)

#%%
plen=np.cumsum(abs(np.roll(d3[0].path,-1)-d3[0].path))
plt.plot(plen,d3[0].values)
plt.scatter(np.cumsum(abs(np.roll(d3[0].path,-1)-d3[0].path)),d3[0].values,s=3)
for i in range(len(d3[0].path)):
    plt.annotate(f"{d3[0].path[i]}",(plen[i],d3[0].values[i].real))

#%%
test.kind=1
big_pic_11=ca.plot3d_grid(test.eqn_stat,(-3,-2),(-0.002,0.002),500,500,np.longcomplex)

#%%
test.kind=3
big_pic=ca.plot3d_grid(test.eqn_stat,(-3,-2),(-0.002,-0.0005),500,500,np.longcomplex)

#%%
test.kind=3
lil_pic=ca.plot3d_grid(test.eqn_stat,(-2.67,-2.6),(-0.0014,-0.0008),500,500,np.longcomplex)

#%%
x,y,z=lil_pic
plt.figure()
phase=np.angle(z)
plt.pcolormesh(x,y,phase,cmap=cm.jet)
plt.colorbar()
plt.contour(x,y,z.real,levels=[0],colors='white',linewidths=3,linestyles='dotted')
plt.contour(x,y,z.imag,levels=[0],colors='green',linewidths=3,linestyles='dotted')

#%%
c=ca.Contour((-2.67,-2.6),(-0.0014,-0.0008),500,500)
ca.circumvent(test.eqn_stat,c)

#%%
test.kind=3
f=lambda x:test.eqn_stat(x-0.005j)
g,r=ca.plot_grid(f,(-3,-2.9),10000)

#%%
walkx3=ca.walk_the_path(lambda z:np.angle(test.eqn_stat(z)),
                        ca.path_around_roots(-2-0.002j,-3-0.0005j,0.1,20,10),
                        0.1)

#%%
walkx2=ca.walk_the_path(lambda z:np.angle(test.eqn_stat(z)),
                        ca.path_around_roots(-2.6-0.0014j,-2.67-0.0008j,0.1,1,1),
                        0.1)

#%%
diffs=np.roll(walkx3[1],-1)-walkx3[1]
plen=np.cumsum(abs(diffs))

#%%
plt.figure()
diffs=np.roll(c1.path,-1)-c1.path
plen=np.cumsum(abs(diffs))
plt.plot(plen,c1.values)
plt.scatter(plen,c1.values,s=3)
for i in range(len(plen)):
    plt.annotate(f"{c1.path[i]}",(plen[i],c1.values[i].real))