# -*- coding: utf-8 -*-

matrix_element=np.dtype([('delta',np.longcomplex),('value',np.longcomplex)])

def matr_els(struct,roots,shift=1,power=1,discr=1000):
    r=np.array([x.mid for x in roots])
    r.sort()
    sl=slice(0,-shift)
    print(r)
    diffs=(np.roll(r,-shift)-r)[sl]
    print(diffs)
    wf=[tuple(
            struct.wavefunc(x,discr,normalize='integrate') for x in t) 
        for t in zip(
            r[sl],np.roll(r,-shift)[sl])]
    me=[st.wf_integrate(wf1,wf2,power=power) for (wf1,wf2) in wf]
    return np.array(list(zip(diffs,me)),dtype=matrix_element)