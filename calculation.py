# -*- coding: utf-8 -*-
"""
Created on Sat May 18 17:00:02 2019

@author: knovich
"""
import numpy as np
import traceback

def plot_grid(func,bounds,points=1000):
    grid=np.linspace(*bounds,points)
    plot=np.array( [func(x) for x in grid] )
    return grid,plot

def condition_segments(func,bounds,condition=(lambda x:x==0),discr=1000):
    grid,vals=plot_grid(func,bounds,discr)
    m_grid=np.ma.masked_where([condition(x) for x in vals],grid)
    return [grid[x][[0,-1]] for x in np.ma.clump_masked(m_grid)]

def array_merge(arr1,arr2,ind,lens):
    merge_list=[]
    i1=0
    i2=0
    for i,n in zip(ind,lens):
        merge_list+=[arr1[i1:i],arr2[i2:i2+n]]
        i1=i
        i2+=n
    merge_list.append(arr1[i:])
    return np.concatenate(merge_list)

TWOPI=2*np.pi

def path_delta(vals):
    return (np.roll(vals,-1)-vals)[:-1]

def nearest_index(arr:np.ndarray,value):
    diff=arr-value
    i=np.argmin(np.abs(diff))
    if diff[i]==0:
        return (i,)
    else:
        mask=diff>0
        i=np.flatnonzero(mask ^ np.roll(mask,-1) )[0]
        return i,i+1

def find_val(arr:np.ndarray,value):
    return int(np.where(arr==value)[0])

def refine_path(func,path,epsilon):
    res=func(path)
    path=path.copy()
    stop=False
    while not stop:
        pd=path_delta(res)
        abs_delta=abs(pd)
        bad_ind=np.flatnonzero((abs_delta > epsilon)&
                (abs_delta < TWOPI-epsilon))
        # print(len(bad_ind))
        # if len(bad_ind)==1:
        #     print(bad_ind)
        #     print(pd[bad_ind[0]-2:bad_ind[0]+3],res[bad_ind[0]-2:bad_ind[0]+3])
        if len(bad_ind)==0:
            stop=True
        else:
            new_arrays=[]
            merge_ind=[]
            merge_len=[]
            for i in bad_ind:
                ins_arr=np.linspace(path[i],path[i+1],
                                    2*abs_delta[i]/epsilon+1)
                if len(np.unique(ins_arr))<len(ins_arr):
                    raise Exception("Precision limit reached in refinery")
                else:
                    ins_arr=ins_arr[1:-1]
                # if len(bad_ind)==1:
                #     print(ins_arr,len(np.unique(np.linspace(path[i],path[i+1],
                #                     2*abs_delta[i]/epsilon+1))))
                new_arrays.append(ins_arr)
                merge_ind.append(i+1)
                merge_len.append(len(ins_arr))
            new_path=np.concatenate(new_arrays)
            if len(np.unique(new_path))<len(new_path):
                raise Exception("Precision limit reached in refinery")
            new_res=func(new_path)
            # if len(bad_ind)==1:
            #     print(new_path)
            res=array_merge(res,new_res,merge_ind,merge_len)
            path=array_merge(path,new_path,merge_ind,merge_len)
    # print('Done refining')
    return path,res

def phase_crosses(vals,epsilon):
    delta=path_delta(vals)
    return np.count_nonzero(delta<epsilon-TWOPI)-np.count_nonzero(delta>TWOPI-epsilon)

def walk_the_path(func,path,epsilon):
    assert(path[0]==path[-1])
    path=path.copy()
    path,res=refine_path(func,path,epsilon)
    crosses=phase_crosses(res,epsilon)
    return crosses,path,res

def path_around_roots(a,b,epsilon,Nroots,aspect):
    nRe=np.pi*Nroots*aspect/(1+aspect)/epsilon
    nIm=np.pi*Nroots/(1+aspect)/epsilon
    return rect_contour(sorted((a.real,b.real)),
                        sorted((a.imag,b.imag)),
                        nRe,nIm)

def rect_contour(bre,bim,pre,pim):
    regrid,imgrid=( np.linspace(*bnd,pts,dtype=np.longdouble) for bnd,pts in ((bre,pre),(bim,pim)) )
    if len(np.unique(regrid))+len(np.unique(imgrid)) < len(regrid)+len(imgrid):
        raise Exception("Precision limit reached in contouring")
    path=np.concatenate((regrid+1j*imgrid[0],
                         1j*imgrid[1:-1]+regrid[-1],
                         regrid[-1::-1]+1j*imgrid[-1],
                         1j*imgrid[-2::-1]+regrid[0]))
    return path

def is_contiguous_integer(arr:np.ndarray):
    return len(np.unique(arr-np.arange(len(arr))))==1

def is_sorted(a:np.ndarray):
    return np.all(a[:-1] <= a[1:])

def is_proper_rect_path(path:np.ndarray):
    if len(path.shape)>1:
        print('Dimension error\n',path)
        return False
    if path[0] != path[-1]:
        print('Tail error\n',path)
        return False
    (re0,im0),(re1,im1)=((path.real.min(),path.imag.min()),(path.real.max(),path.imag.max()))
    side1=np.flatnonzero(path.imag==im0)[:-1]
    side2=np.flatnonzero(path.real==re1)
    side3=np.flatnonzero(path.imag==im1)
    side4=np.flatnonzero(path.real==re0)[1:]
    for x in (side1,side2,side3,side4):
        if not is_contiguous_integer(x):
            print('Continuity error\n',path)
            return False
        if len(x)<2:
            print('Absent side error\n',path)
            return False
    if not ( is_sorted(path[side1].real) and
            is_sorted(path[side2].imag) and
            is_sorted(path[side3[::-1]].real) and
            is_sorted(path[side3[::-1]].real)):
        print('Sorting error\n',path)
        return False
    if ( side1[-1] != side2[0] or
         side2[-1] != side3[0] or
         side3[-1] != side4[0]):
        print('Corners error\n',path)
        return False
    return True

class RectPath:
    def __init__(self,a,b,**kwargs):
        super().__init__()
        if 'epsilon' in kwargs:
            self.epsilon=kwargs['epsilon']
            self.aspect=kwargs['aspect']
            self.Nroots=kwargs['Nroots']
            self.path=path_around_roots(a,b,self.epsilon,self.aspect,self.Nroots)
        elif 'path' in kwargs:
            self.path=kwargs['path']
            if 'values' in kwargs:
                self.values=kwargs['values']
                if len(self.values) != len(self.path):
                    raise Exception('Inconsistency in path-values')
        else:
            raise Exception('No valid arguments specified')
        self.bottomleft=min(a.real,b.real)+1j*min(a.imag,b.imag)
        self.topright=max(a.real,b.real)+1j*max(a.imag,b.imag)
        self.span=abs(self.bottomleft-self.topright)
        self.mid=mid(self.bottomleft,self.topright)
        self.parent=None
        assert(self.bottomleft==self.path.real.min()+1j*self.path.imag.min())
        assert(self.topright==self.path.real.max()+1j*self.path.imag.max())
        assert(is_proper_rect_path(self.path))
        
    def __str__(self):
        return f'{self.bottomleft}..{self.topright}; {len(self.path)}'
    
    def walk_path(self,func):
        crosses,path,res=walk_the_path(func,self.path,self.epsilon)
        self.path=path
        self.values=res
        self.Nroots=crosses
        return crosses
    
    def count_roots(self):
        self.Nroots=phase_crosses(self.values,self.epsilon)
        return self.Nroots
    
    def dissect(self,func):
        chops=chop_path(self,func,self.epsilon)
        for x in chops:
            x.epsilon=self.epsilon
            x.parent=self
            x.count_roots()
        self.children=chops

def all_roots(root:RectPath,func,err,return_dead_ends=True):
    result=[]
    dead_ends=[]
    if not root.walk_path(func):
        return result,dead_ends
    curr_iter=[root]
    while curr_iter:
        next_iter=[]
        for c in curr_iter:
            try:
                c.dissect(func)
            except:
                traceback.print_exc()
                dead_ends.append(c)
                continue
            contour_has_results=False
            for x in c.children:
                if x.Nroots>0:
                    if x.span<err:
                        contour_has_results=True
                        result.append(x)
                    else:
                        next_iter.append(x)
            if not next_iter and not contour_has_results:
                dead_ends.append(c)
        curr_iter=next_iter
        print(len(curr_iter))
    if return_dead_ends:
        return result,dead_ends
    else:
        return result

def chop_path(path:RectPath,func,epsilon):
    #path goes from bottom-left, counter-clockwise
    #Preparation
    p=path.path
    v=path.values
    pv=np.array([p,v])
    (re0,im0),(re1,im1)=((a.real,a.imag) for a in (path.bottomleft,path.topright))
    re_mid=mid(re0,re1)
    im_mid=mid(im0,im1)
    mid_left,mid_right=(a+1j*im_mid for a in (re0,re1))
    mid_bottom,mid_top=(re_mid+1j*b for b in (im0,im1))
    mids=[mid_bottom,mid_right,mid_top,mid_left]
    center=re_mid+1j*im_mid
    
    #Splitting
    # print('Splitting')
    i_corners=[0,find_val(p,re1+1j*im0),find_val(p,re1+1j*im1),find_val(p,re0+1j*im1),len(p)-1]
    corners=[p[i] for i in i_corners]
    sides=[pv[:,i_corners[i]:i_corners[i+1]+1] for i in range(4)]
    i_mids=[nearest_index(s[0],m) for s,m in zip(sides,mids)]
    bridges=[]
    halves=[]
    for i,m,s in zip(i_mids,mids,sides):
        if len(i)==2:
            bridges.append(np.array([
                    s[0,i[0]] , m , s[0,i[1]]
                    ]))
            halves.append( np.split(s,[i[1]],axis=1) )
        else:
            bridges.append(None)
            halves.append( [s[:,:i[0]] , s[:,i[0]+1:]] )
            
    beam_h=np.concatenate((
            np.linspace(mid_left,center,
                        num=max( 
                                (x.shape[1] for x in (halves[0][0],halves[2][1]))
                                ),
                        endpoint=False),
            np.linspace(center,mid_right,
                        num=max( 
                                (x.shape[1] for x in (halves[0][1],halves[2][0]))
                                ),
                        endpoint=False),
            [mid_right]
            ))
    beam_v=np.concatenate((
            np.linspace(mid_bottom,center,
                        num=max( 
                                (x.shape[1] for x in (halves[1][0],halves[3][1]))
                                ),
                        endpoint=False),
            np.linspace(center,mid_top,
                        num=max( 
                                (x.shape[1] for x in (halves[1][1],halves[3][0]))
                                ),
                        endpoint=False),
            [mid_top]
            ))
    if len(np.unique(beam_h))+len(np.unique(beam_v)) < len(beam_h)+len(beam_v):
        raise Exception("Precision limit reached in contouring")
    
    #Calculation
    # print('Calculating bridges')
    i_bridge_split=[]
    for i in range(len(bridges)):
        b=bridges[i]
        if b is not None:
            new_bp,new_bv=refine_path(func,b,epsilon)
            if len(new_bp)>3:
                print('For some reason accuracy dropped within bridge')
            bridges[i]=np.array([new_bp,new_bv])
            i_bridge_split.append(find_val(new_bp,mids[i]))
        else:
            bridges[i]=np.array([[],[]])
            i_bridge_split.append(0)

    # print('Calculating beams')
    beam_h,beam_v=(np.array([*refine_path(func,b,epsilon)]) for b in (beam_h,beam_v))
    i_bh_split,i_bv_split=(find_val(b[0],center) for b in (beam_h,beam_v))
    
    #Splice
    bl=np.concatenate((
            halves[0][0],
            bridges[0][:,1:i_bridge_split[0]],
            beam_v[:,:i_bv_split],
            beam_h[:,i_bh_split::-1],
            bridges[3][:,i_bridge_split[3]+1:-1],
            halves[3][1]
            ),axis=1)
    br=np.concatenate((
            beam_v[:,[0]],
            bridges[0][:,i_bridge_split[0]+1:-1],
            halves[0][1],
            halves[1][0][:,1:],
            bridges[1][:,1:i_bridge_split[1]],
            beam_h[:,-1:i_bh_split:-1],
            beam_v[:,i_bv_split::-1]
            ),axis=1)
    tr=np.concatenate((
            beam_h[:,i_bh_split:],
            bridges[1][:,i_bridge_split[1]+1:-1],
            halves[1][1],
            halves[2][0][:,1:],
            bridges[2][:,1:i_bridge_split[2]],
            beam_v[:,-1:i_bv_split-1:-1]
            ),axis=1)
    tl=np.concatenate((
            beam_h[:,:i_bh_split],
            beam_v[:,i_bv_split:],
            bridges[2][:,i_bridge_split[2]+1:-1],
            halves[2][1],
            halves[3][0][:,1:],
            bridges[3][:,1:i_bridge_split[3]],
            beam_h[:,[0]]
            ),axis=1)
    
    new_chops=[bl,br,tr,tl]
    chop_corners=[(corners[0],center),
                   (mids[0],mids[1]),
                   (center,corners[2]),
                   (mids[3],mids[2])]
    return tuple((RectPath(*c,path=p[0],values=p[1])
                  for c,p in zip(chop_corners,new_chops)  ))


###OLD###

def find_cross(struct,e1,e2,pts):
    grid=np.linspace(e1,e2,pts)
    vals=np.fromiter((struct.eqn_stat(x).real for x in grid),
                     dtype=np.longdouble,count=grid.size)
    s=np.sign(vals)
    m=np.equal(s[:-1],s[1:])
    b=np.ma.compress_cols(np.ma.array((grid[:-1],grid[1:]),
                                           mask=(m,m)))
    return b.T.view(dtype=np.dtype([('x',b.dtype),('y',b.dtype)]))

def plot3d_grid(func,bre,bim,pre=1000,pim=1000,dtype=None,arr=True):
    if dtype==None:
        dtype=np.longdouble
    regrid,imgrid=(np.linspace(*bnd,pts,dtype=np.longdouble) for bnd,pts in ((bre,pre),(bim,pim)) )
    rev,imv=np.meshgrid(regrid,imgrid)
    cv=rev+1j*imv
    if arr:
        res=func(cv)
    else:
        res=np.fromiter( (func(x) for x in np.nditer(cv)),dtype=dtype ).reshape(cv.shape)
    return rev,imv,res

def contour_path(func,bre,bim,pre=1000,pim=1000,dtype=None,arr=True):
    if dtype==None:
        dtype=np.longdouble
    path=rect_contour(bre,bim,pre,pim)
    if arr:
        res=func(path)
    else:
        res=np.fromiter( (func(x) for x in np.nditer(path)),dtype=dtype )
    return path,res

class Contour:
    def __init__(s,bre,bim,pre,pim,**kwargs):
        super().__init__()
        s.re_bound=sorted(bre)
        s.im_bound=sorted(bim)
        s.pre=pre
        s.pim=pim
        s.path=rect_contour(s.re_bound,s.im_bound,pre,pim)
        s.corners=[x+1j*y for x in s.re_bound for y in s.im_bound]
        s.mid=mid(*s.re_bound)+1j*mid(*s.im_bound)
        
    def __str__(s):
        return str(s.corners)
    
    def __repr__(s):
        r=super().__repr__()
        return r+s.__str__()

def circumvent(func,reg:Contour):
    res=func(reg.path[:-1])
    angs=np.angle(res)
    diff=np.roll(angs,1)-angs
    return np.count_nonzero(diff>3)-np.count_nonzero(diff<-3)

def mid(a,b):
    return (a+b)/2

def chop_region(reg:Contour):
    re1,re2=reg.re_bound
    im1,im2=reg.im_bound
    re_mid=mid(re1,re2)
    im_mid=mid(im1,im2)
    return [Contour((re1,re_mid),(im1,im_mid),500,500),
            Contour((re1,re_mid),(im_mid,im2),500,500),
            Contour((re_mid,re2),(im1,im_mid),500,500),
            Contour((re_mid,re2),(im_mid,im2),500,500)]

def dissect_roots(func,region,num_init=0):
    result=[]
    reg_list=chop_region(region)
    if not num_init:
        n_init=circumvent(func,region)
        if n_init==1:
            return region
        elif n_init<1:
            print('No roots found in region',region)
            return []
    else:
        n_init=num_init
    for reg in reg_list:
        n=circumvent(func,reg)
        if n==1:
            result.append(reg)
        elif n>1:
            reg_list+=chop_region(reg)
    if len(result)<n_init:print(f'Lost roots ({n_init-len(result)} out of {n_init}) in region',region)
    if len(result)>n_init:print(f'Extra roots ({-n_init+len(result)} more than {n_init}) in region',region)
    return result

def precision_limit(reg:Contour):
    return (mid(*reg.re_bound) in reg.re_bound) and (mid(*reg.im_bound) in reg.im_bound)

def box_size(reg:Contour):
    rew,imw=(x[1]-x[0] for x in (reg.re_bound,reg.im_bound))
    return ( abs(rew)/abs(reg.mid.real),abs(imw)/abs(reg.mid.imag) )

def tight_box(reg:Contour,tight=0.0001):
    rew,imw=(x[1]-x[0] for x in (reg.re_bound,reg.im_bound))
    return max(abs(rew)/abs(reg.mid.real),abs(imw)/abs(reg.mid.imag))<tight

def broaden_reg(reg:Contour,broad):
    re1,re2,im1,im2=*reg.re_bound,*reg.im_bound
    wre,wim=re2-re1,im2-im1
    return Contour((re1-broad*wre,re2+broad*wre),(im1-broad*wim,im2+broad*wim),reg.pre,reg.pim)

def confine_root(func,region,stop=precision_limit,appr_list=None,broad=0.01):
    if appr_list==None:
        appr_list=[]
    appr_list+=[region]
    reg=[region]
    st=stop(region)
    result=[]
    while not st:
        chops=[]
        for x in reg:
            chops+=chop_region(x)
        next_chops=[]
        for x in chops:
            n=circumvent(func,x)
#            print(n,x)
            if n==1:
                next_chops+=[x]
            elif n>1:
                print('Suspected extra roots in',x)
                extra=dissect_roots(func,x,n)
                next_chops+=extra
        appr_list+=next_chops
#        print(len(next_chops))
        if len(next_chops)>1:
            print('Extra roots',len(next_chops))
        elif len(next_chops)==1:
            pass
        elif len(next_chops)==0:
#            print('Roots disappeared before reaching stop condition')
            return reg+result
        reg=next_chops
        st=True
        new_reg=[]
        for x in reg:
            if not stop(x):
                st=False
                new_reg.append(x)
            else:
#                print('stop for ',x.mid)
                result.append(x)
        reg=new_reg
    return result