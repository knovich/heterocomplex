# -*- coding: utf-8 -*-
"""
Created on Wed May 15 17:46:29 2019

@author: knovich
"""

import ctypes as ct
import numpy as np
class c_longcomplex(ct.Structure):
    _fields_ = ("re", ct.c_longdouble), ("im", ct.c_longdouble)
    
complex_p=ct.POINTER(c_longcomplex)
    
def f90_cmplx(z=0+0j):
    return complex_p(c_longcomplex(z.real,z.imag))

def py_cmplx(pz):
    return np.longcomplex(pz.contents.re+1j*pz.contents.im)
    
def load():
    global _lib
    _lib=ct.CDLL('libtm.dll')
    _lib.eqn_stat.restype=c_longcomplex

def init_calc(struct):
    def as_arg(a):
        return a.ctypes.data_as(ct.POINTER(ct.c_longdouble))
    mass,d,U=(np.array(struct[x]) for x in ['m','d','U'])
    return _lib.init_calc(*(as_arg(x) for x in (mass,d,U)),
                    ct.byref(ct.c_int(struct.size)))
    
def add_calc(struct):
    _calcs.append(struct)
    
def del_calc(h):
    _lib.del_calc(ct.byref(ct.c_int(h)))
    
def eqn_stat(h,en,kind=1):
    res=_lib.eqn_stat(ct.byref(ct.c_int(h)),f90_cmplx(en),ct.byref(ct.c_int(kind)))
    return np.longcomplex(res.re+1j*res.im)

def eqn_stat_arr(h,en,threads=2,kind=1):
    res=np.ndarray(en.shape,dtype=np.longcomplex)
    _lib.eqn_stat_arr(ct.byref(ct.c_int(h)),
                  en.ctypes.data_as(complex_p),
                  res.ctypes.data_as(complex_p),
                  ct.byref(ct.c_int(en.size)),
                  ct.byref(ct.c_int(threads)),
                  ct.byref(ct.c_int(kind)))
    return res

def eqn_ivch(h,en,kind=4):
    res=f90_cmplx()
    _lib.eqn_ivch(ct.byref(ct.c_int(h)),
                  f90_cmplx(en),
                  res,
                  ct.byref(ct.c_int(kind)))
    return py_cmplx(res)

def eqn_bloch(h,en):
    r1,r2=f90_cmplx(),f90_cmplx()
    _lib.eqn_bloch(ct.byref(ct.c_int(h)),f90_cmplx(en),r1,r2)
    return (py_cmplx(r1),py_cmplx(r2))

def wavefunc(h,en,points,t):
    f=np.ndarray((points.size,),dtype=np.longcomplex)
    _lib.wavefunc(ct.byref(ct.c_int(h)),
                  f90_cmplx(en),
                  points.ctypes.data_as(ct.POINTER(ct.c_longdouble)),
                  f.ctypes.data_as(complex_p),
                  ct.byref(ct.c_int(points.size)),
                  ct.byref(ct.c_int(t)))
    return f

def release_dll():
    global _lib,_calcs
    for x in _calcs:
        x.deactivate()
    ct.windll.kernel32.FreeLibrary.argtypes = [ct.wintypes.HMODULE]
    h=_lib._handle
    del _lib
    ct.windll.kernel32.FreeLibrary(h)
    return

load()
_calcs=[]