# -*- coding: utf-8 -*-
"""
Created on Wed Oct  9 18:38:01 2019

@author: knovich
"""

#%%
experiment=[(x,y) for x in [0.005,0.006,0.007,0.008,0.009,0.01][3:-1] for y in [1,1.05,1.1,1.15,1.2,1.25]]
#experiment=[(x,y) for x in [0.005,0.0075,0.01] for y in [1,1.1,1.2]]
grand_total=[]
print(experiment)
res_type=np.dtype([('fld',np.longfloat),('en',np.longfloat),('res',np.ndarray)])
for p in experiment:
    fld=p[0]
    en=p[1]
    period=[(0.5,2,1),(0.5,8,0),(0.5,2,en),(0.5,8,0),(0.5,2,1)]
    result=first_try(period,50,fld,(-0.001,-1e-6))
    grand_total+=[(fld,en,result)]
    print(grand_total[-1])
result_arr=np.array(grand_total,dtype=res_type)
np.save('res03',result_arr)

#%%
%matplotlib qt
final=np.load('le_gran_result.npy',allow_pickle=True)

def m_el(r1,r2,struct):
    wf1,wf2=(struct.wavefunc(x,1000,normalize='integrate') for x in (r1,r2))
    return st.wf_integrate(wf1,wf2,1)

def func_to_ind(ind,func):
    result={}
    r=final[ind]
    roots=r['res']['value']
    struct=exp_str(r['fld'],r['en'])
    for t in tr_types:
        l=transitions[ind][t]
        result[t]=np.array([( (roots[x[0]]-roots[x[1]]).real,
                              func(roots[x[0]],roots[x[1]],struct) ) for x in l])
    return result

mat_els=lambda x:func_to_ind(x,m_el)
Ws=lambda x:func_to_ind(x,W_roots)

#%%
r=final[5,5]
e=exp_str(r['fld'],r['en'])
me=[]
for x in r['res']:
    for y in r['res']:
        wf1=e.wavefunc(x['value'],1000,normalize='integrate')
        wf2=e.wavefunc(y['value'],1000,normalize='integrate')
        me+=[(x['value']-y['value'],st.wf_integrate(wf1,wf2,1))]
        
#%%
r=final[5,0]
e=exp_str(r['fld'],r['en'])
plt.figure()
for x in r['res']:
    wf1=e.wavefunc(x['value'],1000,normalize='integrate')
    plt.plot(wf1[0],abs(wf1[1]))

#%%
for ind in [(x,y) for x in [5] for y in [0]]:
    plt.figure()
    w=Ws(ind)
    keys=tr_types
    for k in keys:
        try:
            v=w[k]
            plt.scatter(v[:,0],v[:,1].real,label=f"{ind} {k} {v.shape[0]}",s=100)
            for i in enumerate(v):
                plt.annotate(f"{transitions[ind][k][i[0]]} {i[1]}",i[1].real)
        except:
            pass
    plt.legend(loc='best')
    
#%%
str50=exp_str(*final[5,0][['fld','en']])
roots50=final[5,0]['res']['value']

#%%
delta=2e-5+1e-8j
W_roots(roots50[2]-delta,roots50[0]+delta,str50)

#%%
res=final[5,0]
struct=exp_str(res['fld'],res['en'])
r1,r2=res['res'][[5,6]]
e1,e2=(x['value'].real+1j*(x['value'].imag)*(1-x['im_err']) for x in (r1,r2))
print(W_roots(e2,e1,struct))

#%%
import cxroots
test=[(0.5,2,1),(0.5,8,0),(0.5,2,1)]
sl=st.Structure(test,field=0.01,discr=30,last=True,repeat=100,threads=1)
c=cxroots.Rectangle([-4.6,-4.5],[-0.001,-0.0001])
ctr=ca.Contour([-4.6,-4.5],[-0.001,-0.0001],500,500)
plt.plot(*sl.Uprofile)
print(ca.circumvent(sl.eqn_stat,ctr))
print(sl.eqn_stat(8+9j))
print(c.count_roots(sl.eqn_stat))
r=c.roots(sl.eqn_stat)
print(r)


