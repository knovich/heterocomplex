# -*- coding: utf-8 -*-
"""
Created on Tue May 28 15:57:50 2019

@author: knovich
"""
import calculation as ca
import structure as st
import matplotlib.pyplot as plt
import matplotlib.cm as cm
import matplotlib.colors as clr
import numpy as np

def bloch_zones(struct,discr=1000):
    us=struct._struct['U']
    interval=(us.min(),us.max())
    f=lambda x:struct.eqn_bloch(x)[1].imag
    return ca.condition_segments(f,interval,
                                 condition=lambda x:abs(x)<=1e-12,discr=discr)

matrix_element_type=np.dtype([('delta',np.longcomplex),('value',np.longcomplex)])
res_type=np.dtype([('fld',np.longfloat),('en',np.longfloat),('res',np.ndarray)])
root_type=np.dtype([('value',np.longcomplex),('re_err',np.longfloat),('im_err',np.longfloat)])

def matr_els(struct,roots,shift=1,power=1,discr=1000):
    r=roots['value']
    sl=slice(0,-shift)
    diffs=(np.roll(r,-shift)-r)[sl]
    wf=[tuple(
            struct.wavefunc(x,discr,normalize='integrate') for x in t)
        for t in zip(
            r[sl],np.roll(r,-shift)[sl])]
    me=[st.wf_integrate(wf1,wf2,power=power) for (wf1,wf2) in wf]
    return np.array(list(zip(diffs,me)),dtype=matrix_element_type)

w_factor=906_169

def W_roots(r1,r2,struct,eps=10):
    wf1,wf2=(struct.wavefunc(x,1000,normalize='integrate') for x in (r1,r2))
    me=st.wf_integrate(wf1,wf2,power=1)
    return abs(me)**2*(r1-r2).real**3*w_factor*eps

def roots_from_contours(root_list):
    result=np.ndarray((len(root_list),),dtype=root_type)
    result['value']=[x.mid for x in root_list]
    result[['re_err','im_err']]=[(  (x.topright-x.bottomleft).real,
                                    (x.topright-x.bottomleft).imag) for x in root_list]
    result.sort(order='value')
    return result

def plot_wfs(struct:st.Structure,roots):
    zones=bloch_zones(struct.period)
    field=struct.field
    f=plt.get_fignums()
    if f:
        fn=max(f)+1
    else:
        fn=1
    plt.figure(fn)
    plt.plot(*struct.Uprofile)
    dd=np.linspace(0,struct.len,10000)
    for x in zones:
        plt.plot(dd,x[0]-field*dd,color='green')
        plt.plot(dd,x[1]-field*dd,color='red')
    for i,r in enumerate(roots):
        wf=struct.wavefunc(r['value'],1000,normalize='integrate')
        wfm=max(abs(wf[1]))
        plt.plot(wf[0],
                 r['value'].real+abs(wf[1])/wfm*max(struct.period._struct['U']),
                 label=f"{i} {r['value']} {r['re_err']} {r['im_err']}")
    plt.legend(loc='best')
    plt.title(str(field)+' '+str(struct.period._struct['U']))
    plt.show()

def first_try(period,repeat,field,im_bounds,max_roots=10):
    struct=st.Structure(period,field=field,discr=30,last=True,repeat=repeat,threads=4)
    plen=struct.period.len
    mid=struct.len/2.0
    zones=bloch_zones(struct.period)
    minzone=zones[0][0]
    maxzone=zones[-1][-1]
    cntr=ca.Contour((minzone-field*(mid+2*plen),
                     maxzone-field*(mid-2*plen)),im_bounds,500,500)
    cl=ca.dissect_roots(struct.eqn_stat,cntr)
    cl=sorted(cl,key=lambda x:x.mid.real)
    root_c=[]
    for x in cl[:max_roots]:
#        print('root in',x,' ',ca.circumvent(struct.eqn_stat,x))
        rl=ca.confine_root(struct.eqn_stat,x,stop=ca.tight_box)
        root_c+=rl
    roots=roots_from_contours(root_c)
    plot_wfs(struct,roots)
    return roots

def extract_series(res,struct):
    r=res.copy()
    mask=[False]
    result=[]
    for i,x in enumerate(r):
        if mask[i]:
            continue
        tmp=(r-x).real/struct.field/struct.period.len
        newm=np.logical_and([close_to_int(v) != 0 for v in tmp],np.logical_not(mask))
        newm[i]=True
        mask=np.logical_or(newm,mask)
        if np.count_nonzero(newm)>1:
            serie=r[newm]
            result+=[serie]
    return result

def exp_str(field,en):
    period=[(0.5,2,1),(0.5,8,0),(0.5,2,en),(0.5,8,0),(0.5,2,1)]
    struct=st.Structure(period,field=field,discr=30,last=True,repeat=50,threads=4)
    return struct

def plot_result(res,ind=Ellipsis):
    plot_wfs(exp_str(res['fld'],res['en']),res['res'][ind])

def good_enough(root:root_type):
    return root['re_err','im_err'].max()<0.001

def close_to_int(a,tol=0.01):
    i=round(a)
    if abs(a-i)<tol:
        return i
    else:
        return 0
    
def close_to_int_b(a,tol=0.1):
    i=round(a)
    if abs(a-i)<tol:
        return True
    else:
        return False
    
def visualize_eqn(f,b1,b2,zf=np.angle,arr=True):
    plt.figure()
    re_b=sorted((b1.real,b2.real))
    im_b=sorted((b1.imag,b2.imag))
    x,y,z=ca.plot3d_grid(f,re_b,im_b,300,150,np.longcomplex,arr)
    plt.pcolormesh(x,y,zf(z),cmap=cm.jet)
    plt.colorbar()
    plt.contour(x,y,z.real,levels=[0],colors='white')
    plt.contour(x,y,z.imag,levels=[0],colors='black')
    plt.show()

#period=[(0.5,2,1),(0.5,8,0),(0.5,2,1)]
#test=st.Structure(period,field=0.01,discr=30,last=True,repeat=50)
#c=ca.Contour((-1.5,1),(-0.01,-1e-20),250,250)
#plt.figure(k)
#x,y,z=ca.plot3d_grid(sl.eqn_stat,(-3,-2),(-0.001,0),300,150,np.longcomplex)
#plt.pcolormesh(x,y,np.angle(z),cmap=cm.jet)
#plt.show()
#print(ca.circumvent(sl.eqn_stat,c))
#cl=ca.dissect_roots(sl2.eqn_stat,c)
#print(len(cl))
#results2=[]
#approxes2=[]
#for x in cl:
#    al=[]
#    rl=ca.confine_root(sl2.eqn_stat,x,stop=ca.tight_box,appr_list=al)
#    results2+=rl
#    approxes2+=[al]
#    print('Roots:',[y.mid for y in rl])
#    print('Approximations: ...',[y.mid for y in al[-5:]])