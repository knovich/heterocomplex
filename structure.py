# -*- coding: utf-8 -*-
"""
Created on Wed May 15 14:12:18 2019

@author: knovich
"""

import numpy as np
import libtm

ltype=np.dtype([('m',np.longfloat),('d',np.longfloat),('U',np.longfloat)])

def subdivide(layer,num):
    return np.full((num,),np.array((layer['m'],layer['d']/num,layer['U']),
                    dtype=ltype))

def create_mesh(struct,num):
    x=np.concatenate([subdivide(layer,num) for layer in struct[1:-1]])
    return np.concatenate(([struct[0]],x,[struct[-1]]))

def apply_field(struct,field,frac=0.5,last=False):
    x=np.cumsum(struct['d'])-struct['d']*(1-frac)-struct['d'][0]
    a=struct.copy()
    end=a.size if last else -1
    a['U'][1:end]=a['U'][1:end]-field*x[1:end]
    return a

class Structure:
    def __init__(self,struct,field=0,discr=1,last=False,kind=3,repeat=1,threads=2):
        super().__init__()
        x=np.array(struct,dtype=ltype)
        if repeat>1:
            self.period=Structure(struct,kind=kind)
            self._struct=np.append(np.tile(x[:-1],repeat),
                                   x[-1])
        else:
            self._struct=x
        if field:
            self._struct=apply_field(create_mesh(self._struct,discr),
                                     field,last=last)
        ds=np.concatenate(([0],np.cumsum(self._struct['d'])))
        self.Uprofile=(np.ravel([ds[:-1],ds[1:]],order='F'),
                       np.array([self._struct['U'],
                                 self._struct['U']]).flatten(order='F'))
        self.activate()
        self.kind=kind
        self.threads=threads
        self.repeat=repeat
        if repeat>1:
            self.len=sum(self._struct['d'])
        else:
            self.len=sum(self._struct['d'][:-1])
        self.field=field
        
    def activate(self):
        self._handle=libtm.init_calc(self._struct)
        libtm.add_calc(self)
        self._active=True
        
    def __del__(self):
        libtm.del_calc(self._handle)
        
    def deactivate(self):
        self._active=False
        
    def check(self):
        return self._active
        
    def eqn_stat(self,en,kind=None):
        if kind==None:
            kind=self.kind
        threads=self.threads
        if isinstance(en,np.ndarray):
            return libtm.eqn_stat_arr(self._handle,en,threads,kind)
        else:
            return libtm.eqn_stat(self._handle,en,kind)
    
    def eqn_bloch(self,en):
        return libtm.eqn_bloch(self._handle,en)
    
    def eqn_ivch(self,en,kind=None):
        if kind==None:
            kind=4
        return libtm.eqn_ivch(self._handle,en,kind)
    
    def wavefunc(self,en,points,start=0,end=0,kind=None,normalize=None):
        if kind==None:
            kind=self.kind
        pts=np.linspace(start,sum(self._struct['d'])+end,points)
        wf=libtm.wavefunc(self._handle,en,pts,kind)
        if normalize=='max':
            norm=max(abs(wf))
            wf=wf/norm
        elif normalize=='integrate':
            norm=max(abs(wf))
            wf=wf/norm
            norm=np.sqrt(wf_integrate((pts,wf),(pts,wf)))
            wf=wf/norm
        return pts,wf

def print_return(func):
    def ret(*args,**kwargs):
        res=func(*args,**kwargs)
        print(res)
        return(res)
    return ret

class IntegrateException(Exception):pass

#@print_return
def wf_integrate(wf1,wf2,power=0):
    if np.all(wf1[0]==wf2[0]):
        return np.sum(wf1[1]*wf2[1].conj()*wf1[0]**power)
    else:
        raise IntegrateException
        
def superlattice(period,num,**kwarg):
    if isinstance(period,Structure):
        return Structure(np.append(np.tile(period._struct[:-1],num),
                                   period._struct[-1]),**kwarg)
    else:
        x=np.array(period,dtype=ltype)
        return Structure(np.append(np.tile(x[:-1],num),
                                   x[-1]),**kwarg)