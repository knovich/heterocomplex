from bl2nb import *
#%%
e1=exp_discr(field=0.004,num=50)
e2=exp_discr(field=0.02,num=50)
e3=exp_discr(field=0.004,num=100)
e4=exp_discr(field=0.02,num=100)
e_d=(e1,e2,e3,e4)
r_d=[]
for e in e_d:
    r_d.append(prelim_experiment(e,job_prelim))

s_d=[]
for e,r in zip(e_d,r_d):
    s_d.append(
        [sort_series(
            bl.extract_series(
                bl.roots_from_contours(r[i])['value'],
                e[i]))
         for i in range(len(e))]
        )

with open('exp_discr.dat','wb') as f:
    pickle.dump((e_d,s_d),f)

#%%
ens=np.linspace(1,1.5,20)
l=[]
for x in ens:
    period=[(0.5,2,1),(0.5,8,0),(0.5,2,x),(0.5,8,0),(0.5,2,1)]
    s1=st.Structure(period,field=0.004,discr=20,last=True,repeat=50)
    l.append(bl.bloch_zones(s1.period))
plt.plot(ens[1:],[x[1][0]-x[0][1] for x in l[1:]])
plt.scatter(ens[1:],[x[1][0]-x[0][1] for x in l[1:]])

#%%
e_f=exp_field(num=15)
r_f=prelim_experiment(e_f,lambda x:job_prelim(x,num=5))
s_f=[sort_series(bl.extract_series(bl.roots_from_contours(r_f[i])['value'],
                                   e_f[i])) 
     for i in range(len(e_f))]

#%%
e_f3=exp_field(num=15,rep=200)
r_f3=prelim_experiment(e_f3,lambda x:job_prelim(x,num=5))
s_f3=[sort_series(bl.extract_series(bl.roots_from_contours(r_f3[i])['value'],
                                    e_f3[i])) 
     for i in range(len(e_f3))]

#%%
ens=np.linspace(1.01,1.5,10)
gap_res=[]
for en in ens:
    period=[(0.5,2,1),(0.5,8,0),(0.5,2,en),(0.5,8,0),(0.5,2,1)]
    ex=exp_field(num=15,period=period,rep=50)
    res=prelim_experiment(ex,lambda x:job_prelim(x,num=5))
    ser=[sort_series(bl.extract_series(bl.roots_from_contours(res[i])['value'],
                                       ex[i]))
         for i in range(len(ex))]
    gap_res.append((en,ex,ser))

with open('exp_gap.dat','wb') as f:
    pickle.dump(gap_res,f)

#%%
with open('exp_gap.dat','rb') as f:
    gap_res=pickle.load(f)

for x in gap_res:
    for y in x[1]:
        y.activate()
        y.period.activate()
        
#%%
with open('exp_nogap.dat','rb') as f:
    nogap_res=pickle.load(f)

for y in nogap_res[1]:
    y.activate()
    y.period.activate()

#%%
plt.style.use('default')
plt.style.use('my.mplstyle')
plt.rcParams['figure.figsize'] = (8,8)

#%%
plt.figure(figsize=(8,8))
st1=nogap_res[1][0].period
plt.plot(*st1.Uprofile,'k')
plt.ylabel('E, eV')
plt.xlabel(r'z, $\AA$')
plt.xlim(0,10)
plt.ylim(0,1.2)
plt.xticks(range(0,11,5))
plt.yticks(np.arange(0,1.1,0.25))
ax=plt.gca()
ax.twiny()
z1=bl.bloch_zones(st1)
f=lambda x: abs(st1.eqn_bloch(x)[0].real)
for z in z1:
    en,kd=ca.plot_grid(f,(0.95*z[0],1.01*z[1]),points=1000)
    plt.plot(kd,en)
plt.xticks((0,np.pi/2,np.pi),('0',r'$\pi/2$',r'$\pi$'))
plt.xlabel(r'K$\cdot$d')
plt.tight_layout()
plt.savefig('Fig3.png')

#%%
plt.figure(figsize=(8,8))
st1=gap_res[-1][1][0].period
plt.plot(*st1.Uprofile,'k')
plt.ylabel('E, eV')
plt.xlabel(r'z, $\AA$')
plt.xlim(0,20)
plt.ylim(0,1.5)
plt.xticks(range(0,21,5))
plt.yticks(np.arange(0,1.6,0.5))
ax=plt.gca()
ax.twiny()
z1=bl.bloch_zones(st1)
f=lambda x: abs(st1.eqn_bloch(x)[0].real)
for z in z1[:-1]:
    en,kd=ca.plot_grid(f,(0.95*z[0],1.01*z[1]),points=1000)
    plt.plot(kd,en)
plt.xticks((0,np.pi/2,np.pi),('0',r'$\pi/2$',r'$\pi$'))
plt.xlabel(r'K$\cdot$d')
plt.tight_layout()
plt.savefig('Fig6.png')

#%%
plt.rcParams['lines.linewidth'] = 1
plt.rcParams['figure.figsize'] = (8,8)
st1=nogap_res[1][-5]
r1=nogap_res[2][-5]
rl1=np.array([r1[0][0],r1[0][1]])[-1::-1]
bl.plot_wfs(st1,root_t_from_series(rl1))
plt.ylabel('E, eV')
plt.xlabel(r'z, $\AA$')
plt.ylim(-9,-4.5)
plt.xlim(445,725)
plt.xticks([])
plt.yticks([])
plt.title('')
stls=['-','-']
lws=[2,2]
clrs=['k','r','k','r']
lbs=[r'$E_0+eFd$',r'$E_0$']
for i,x in enumerate(plt.gca().get_lines()[3:]):
    x.set_linewidth(lws[i])
    x.set_color(clrs[i])
    x.set_linestyle(stls[i])
    x.set_label(lbs[i])
plt.legend()
#plt.legend(['','2','3','4','5','6','7','8'])
plt.tight_layout()
plt.savefig('Fig4.png')
plt.rcParams['lines.linewidth'] = 3

#%%
plt.rcParams['lines.linewidth'] = 1
plt.rcParams['figure.figsize'] = (8,8)
st1=gap_res[6][1][-5]
r1=gap_res[6][2][-5]
rl1=np.array([r1[0][0],r1[1][0],r1[0][1],r1[1][1]])[-1::-1]
bl.plot_wfs(st1,root_t_from_series(rl1))
plt.ylabel('E, eV')
plt.xlabel(r'z, $\AA$')
plt.ylim(-9,-4.5)
plt.xlim(445,725)
plt.xticks([])
plt.yticks([])
plt.title('')
stls=['--','--','-','-']
lws=[1,1,2,2]
clrs=['k','r','k','r']
lbs=[r'$E_0+1.53eFd$',r'$E_0+eFd$',r'$E_0+0.53eFd$',r'$E_0$']
for i,x in enumerate(plt.gca().get_lines()[7:]):
    x.set_linewidth(lws[i])
    x.set_color(clrs[i])
    x.set_linestyle(stls[i])
    x.set_label(lbs[i])
plt.legend()
#plt.legend(['','2','3','4','5','6','7','8'])
plt.tight_layout()
plt.savefig('Fig8.png')
plt.rcParams['lines.linewidth'] = 3

# %%
x=range(10,110,10)
#lws=[1,1,2,2]
fmts=['r-','k-','g:','b:']
wdths=[2,2,3,3]
plt.figure()
plt.yscale('log')
plt.xscale('log')
plt.xlabel('Discretization')
plt.ylabel(r'$\Delta E/\delta E$')
lbs=[r'$N=50,\delta E=0.04\mathrm{eV}$',
     r'$N=50,\delta E=0.2\mathrm{eV}$',
     r'$N=100,\delta E=0.04\mathrm{eV}$',
     r'$N=100,\delta E=0.2\mathrm{eV}$']
for es,ss,j in zip(e_d,s_d,range(4)):
    err=[-(ss[i+1][0]-ss[i][0])[0].real/(ss[0][0][1]-ss[0][0][0]).real 
         for i in range(len(ss)-1)]
    plt.plot(x[:-1],err,fmts[j],
             label=lbs[j],linewidth=wdths[j])
plt.ylim(top=1e-4)
plt.xticks([10,100],['10','100'])
plt.gca().xaxis.set_minor_formatter(mpl.ticker.NullFormatter())
plt.legend()
plt.tight_layout()
plt.savefig('Fig1.png')

# %%
x=range(10,110,10)
#lws=[1,1,2,2]
fmts=['r-','k-','g:','b:']
wdths=[2,2,3,3]
plt.figure()
plt.yscale('log')
plt.xscale('log')
plt.xlabel('Discretization')
plt.ylabel(r'$\Delta W/W$')
lbs=[r'$N=50,\delta E=0.04\mathrm{eV}$',
     r'$N=50,\delta E=0.2\mathrm{eV}$',
     r'$N=100,\delta E=0.04\mathrm{eV}$',
     r'$N=100,\delta E=0.2\mathrm{eV}$']
for es,ss,j in zip(e_d,s_d,range(4)):
    Ws=[bl.W_roots(ss[i][0][3],ss[i][0][2],es[i])
        for i in range(len(es))]
    err=[abs(Ws[i+1]-Ws[i])/Ws[i+1]
         for i in range(len(es)-1)]
    plt.plot(x[:-1],err,fmts[j],
             label=lbs[j],linewidth=wdths[j])
plt.ylim(top=1e-4)
plt.xticks([10,100],['10','100'])
plt.gca().xaxis.set_minor_formatter(mpl.ticker.NullFormatter())
plt.legend()
plt.tight_layout()
plt.savefig('Fig2.png')

#%%
plot_Ws(nogap_res[1],nogap_res[2])
add_W_est(plt.gcf().number,0.6,10)
plt.legend(['Single','Double','Triple','Estimate'])
plt.ylabel('W')
plt.xlabel(r'$\delta E$, eV')
for x in range(3,9):
    plt.axvline(0.6/x,color='gray',linewidth=1)
    plt.text(0.6/x-0.005,2e7,r'$\delta U'+f'/{x}$',rotation=90)
plt.tight_layout()
plt.savefig('Fig5.png')

#%%
n=plot_Ws_err_cross(gap_res[2][1],gap_res[2][2],err=False)
plot_Ws_err_cross(gap_res[5][1],gap_res[5][2],fignum=n,err=False)
add_W_est(n,0.6,10)
plt.ylabel('W')
plt.xlabel(r'$\delta E$, eV')
lws=[3,3,3,3,3]
clrs=['k','k','r','r','b']
stls=['-','--','-','--','--']
lbs=[r'$\delta V=0.12$, type I',
     r'$\delta V=0.12$, type II',
     r'$\delta V=0.28$, type I',
     r'$\delta V=0.28$, type II',
     r'Estimate for $\delta V=0$']
for i,x in enumerate(plt.gca().get_lines()):
    x.set_linewidth(lws[i])
    x.set_color(clrs[i])
    x.set_linestyle(stls[i])
    x.set_label(lbs[i])
plt.legend()
plt.tight_layout()
plt.savefig('Fig9.png')

#%%
n=plot_Ws_err_cross(gap_res[1][1],gap_res[1][2],err=False)
plot_Ws_err_cross(gap_res[7][1],gap_res[7][2],fignum=n,err=False)
add_W_est(n,0.6,10)
plt.ylabel('W')
plt.xlabel(r'$\delta E$, eV')
lws=[3,3,3,3,3]
clrs=['k','k','r','r','b']
stls=['-','--','-','--','--']
lbs=[r'$\delta V=0.06$, type I',
     r'$\delta V=0.06$, type II',
     r'$\delta V=0.39$, type I',
     r'$\delta V=0.39$, type II',
     r'Estimate for $\delta V=0$']
for i,x in enumerate(plt.gca().get_lines()):
    x.set_linewidth(lws[i])
    x.set_color(clrs[i])
    x.set_linestyle(stls[i])
    x.set_label(lbs[i])
plt.legend()
plt.tight_layout()
plt.savefig('FigA.png')

#%%
zones=np.array([bl.bloch_zones(x[1][0].period)[:2] 
                for x in gap_res]).reshape((len(gap_res),4))
dv=np.array([x[0]-1 for x in gap_res])
plt.figure()
plt.xlabel(r'$\delta V$, eV')
plt.ylabel('U, eV')
plt.xlim(0,0.5)
plt.ylim(0,1)
plt.fill_between(dv,zones[:,0],zones[:,1])
plt.fill_between(dv,zones[:,2],zones[:,3])
plt.gca().twinx()
plt.ylabel('Gap width, eV')
plt.plot(dv,zones[:,2]-zones[:,1],'k--')
plt.ylim(0,0.06)
plt.arrow(0.4,0.05,0.05,0,ls='--',width=0,head_width=0.001,head_length=0.01,fc='k')
plt.tight_layout()
plt.savefig('Fig7.png')

#test
