    module libtm

    use,intrinsic:: iso_c_binding
    implicit none
    private
    public::initcalc,eqn_stat,eqn_bloch,delcalc

    integer,parameter::pr=c_long_double
    integer,parameter::pc=c_long_double_complex

    real(pr),parameter::HBAR=2.759_pr
    
    type structure
        integer(c_int)          ::  N
        real(pr),allocatable    ::  mass(:),D(:),U(:)
    end type structure
    
    integer(c_int)::handle=0
    type(structure),allocatable::strs(:)

    contains
    
    
    integer(c_int) function initcalc(mass_arr,D_arr,U_arr,N_val) bind(c,name='init_calc')
    !DEC$ ATTRIBUTES DLLEXPORT::initcalc
    real(pr),intent(in)                 ::  mass_arr(N_val), D_arr(N_val), U_arr(N_val)
    integer(c_int),intent(in)           ::  N_val
    type(structure)                 ::  new_str
    
    handle= handle+1
    new_str%N= N_val
    allocate(new_str%mass(N_val),new_str%D(N_val),new_str%U(N_val))
    new_str%mass= mass_arr
    new_str%D= D_arr
    new_str%U= U_arr
    if(.not. allocated(strs))then
        allocate(strs(1),source=[new_str])
    else
        strs= [strs,new_str]
    end if
    initcalc=handle
    end function initcalc
    
    
    subroutine delcalc(handle) bind(c,name='del_calc')
    integer(c_int),intent(in)           ::  handle
    !DEC$ ATTRIBUTES DLLEXPORT::delcalc
    
    strs(handle)%N= 0
    deallocate(strs(handle)%mass,strs(handle)%D,strs(handle)%U)
    end subroutine delcalc
    
    
    !DEC$ ATTRIBUTES FORCEINLINE :: populate_kvec
    subroutine populate_kvec(h,e,k_vec,t)
    integer(c_int),intent(in)           ::  h,t
    complex(pc),intent(in)              ::  e
    complex(pc),intent(out)             ::  k_vec(:)
    integer(c_int)                  ::      i
    
    select case (t)
    case (1)
        k_vec= (0._pr,1._pr)*sqrt(2_pr*strs(h)%mass*(e-strs(h)%U))/HBAR
    case (2)
        k_vec= sqrt(2_pr*strs(h)%mass*(strs(h)%U-e))/HBAR
    case (3)
        where(strs(h)%U<real(e))
            k_vec= (0._pr,1._pr)*sqrt(2_pr*strs(h)%mass*(e-strs(h)%U))/HBAR
        elsewhere
            k_vec= (0._pr,-1._pr)*sqrt(2_pr*strs(h)%mass*(e-strs(h)%U))/HBAR
        end where
    case (4)
        where(strs(h)%U<real(e))
            k_vec= sqrt(2_pr*strs(h)%mass*(e-strs(h)%U))/HBAR
        elsewhere
            k_vec= (0._pr,1._pr)*sqrt(2_pr*strs(h)%mass*(strs(h)%U-e))/HBAR
        end where
    end select
    end subroutine populate_kvec
    
    
    !DEC$ ATTRIBUTES FORCEINLINE :: Tm_Prod
    function Tm_Prod(h,e,t) result(t_p)
    integer(c_int),intent(in)           ::  h,t
    complex(pc),intent(in)              ::  e
    integer                         ::      i
    complex(pc)                     ::      t_p(2,2), k_vec(strs(h)%N)
    
    call populate_kvec(h,e,k_vec,t)
    t_p= Transfer_Matrix(h,1,k_vec)
    do i= 2,strs(h)%N-1
        t_p= matmul(Transfer_Matrix(h,i,k_vec),t_p)
    end do
    end function Tm_Prod
    
    
    !DEC$ ATTRIBUTES FORCEINLINE :: Tm_Prod_ivch
    function Tm_Prod_ivch(h,e,t,k_vec) result(t_p)
    integer(c_int),intent(in)           ::  h,t
    complex(pc),intent(in)              ::  e
    complex(pc)                         ::  k_vec(strs(h)%N)
    integer                         ::      i
    complex(pc)                     ::      t_p(2,2)
    
    t_p= Transfer_Matrix_ivch(h,1,k_vec)
    do i= 2,strs(h)%N-1
        t_p= matmul(Transfer_Matrix_ivch(h,i,k_vec),t_p)
    end do
    end function Tm_Prod_ivch


    complex(pc) function eqn_stat(h,e,t) bind(c,name='eqn_stat')
    !DEC$ ATTRIBUTES DLLEXPORT::eqn_stat
    integer(c_int),intent(in)           ::  h,t
    complex(pc),intent(in)              ::  e
    complex(pc)                     ::      t_p(2,2)

    t_p= Tm_Prod(h,e,t)
    select case (t)
    case (1)
        eqn_stat=t_p(1,1)
    case (3)
        eqn_stat=t_p(2,2)
    case (2)
        eqn_stat=t_p(1,1)
    end select
    end function eqn_stat
    
    
    subroutine eqn_stat_arr(h,e,res,sz,thr,t) bind(c)
    !DEC$ ATTRIBUTES DLLEXPORT::eqn_stat_arr
    integer(c_int),intent(in)           ::  h,sz,thr,t
    complex(pc),intent(in)              ::  e(sz)
    complex(pc),intent(inout)           ::  res(sz)
    complex(pc)                     ::      t_p(2,2)
    integer(c_int)                  ::      i

    !$OMP PARALLEL NUM_THREADS(thr),SHARED(h,res,e)
    !$OMP DO SCHEDULE(STATIC),PRIVATE(t_p)
    do i=1,sz
        t_p= Tm_Prod(h,e(i),t)
        select case (t)
        case (1)
            res(i)=t_p(1,1)
        case (3)
            res(i)=t_p(2,2)
        case (2)
            res(i)=t_p(1,1)
        end select
    end do
    !$OMP END DO NOWAIT
    !$OMP END PARALLEL
    end subroutine eqn_stat_arr
    
    
    subroutine eqn_bloch(h,e,r1,r2) bind(c,name='eqn_bloch')
    !DEC$ ATTRIBUTES DLLEXPORT::eqn_bloch
    integer(c_int),intent(in)           ::  h
    complex(pc),intent(in)              ::  e
    complex(pc),intent(out)             ::  r1,r2
    complex(pc)                     ::      t_p(2,2), a, d

    t_p= Tm_Prod(h,e,1)
    a= (t_p(1,1)+t_p(2,2))/2_pr
    d= t_p(1,1)*t_p(2,2)-t_p(1,2)*t_p(2,1)
    r1= log(a-sqrt(a**2-d))*(0_pr,-1_pr)
    r2= log(a+sqrt(a**2-d))*(0_pr,1_pr)
    end subroutine eqn_bloch
    
    subroutine eqn_ivch(h,e,res,t) bind(c,name='eqn_ivch')
    !DEC$ ATTRIBUTES DLLEXPORT::eqn_ivch
    integer(c_int),intent(in)           ::  h,t
    complex(pc),intent(in)              ::  e
    complex(pc),intent(out)             ::  res
    complex(pc)                     ::      t_p(2,2),k_vec(strs(h)%N),factor
    integer                         ::      n

    call populate_kvec(h,e,k_vec,t)
    n= strs(h)%N
    t_p= Tm_Prod_ivch(h,e,t,k_vec)
    factor= (0,1)*k_vec(1)/k_vec(n)*strs(h)%mass(n)/strs(h)%mass(1)
    res= t_p(1,1)-(0,1)*t_p(1,2)+factor*(t_p(2,1)-(0,1)*t_p(2,2))
    end subroutine eqn_ivch
    
    
    subroutine wavefunc(h,e,pts,f,discr,t) bind(c,name='wavefunc')
    !DEC$ ATTRIBUTES DLLEXPORT::wavefunc
    integer(c_int),intent(in)           ::  h,discr,t
    complex(pc),intent(in)              ::  e
    real(pr),intent(in)                 ::  pts(discr)
    complex(pc),intent(inout)           ::  f(discr)
    complex(pc)                     ::      ab(2,1:strs(h)%N),k_vec(strs(h)%N)
    real(pr)                        ::      bnds(1:strs(h)%N+1),dx(discr)
    integer                         ::      lnum(discr),i,l
    
    call populate_kvec(h,e,k_vec,t)
    select case (t)
    case (3)
        ab(:,1)= [0,1]
    case (1,2)
        ab(:,1)= [1,0]
    end select
    do i= 2,strs(h)%N
        ab(:,i)= matmul(Transfer_Matrix(h,i-1,k_vec),ab(:,i-1))
    end do
    bnds(1)= 0
    do i= 2,strs(h)%N
        bnds(i)= bnds(i-1)+strs(h)%D(i-1)
    end do
    bnds(strs(h)%N+1)= pts(discr)*2_pr
    l=1
    do i= 1,discr
        do while (bnds(l+1) <= pts(i))
            l= l+1
        end do
        lnum(i)=l
    end do
    
    dx(:)= pts(:)-bnds(lnum(:))
    f(:)= ab(1,lnum(:))*exp(k_vec(lnum(:))*dx(:))+ab(2,lnum(:))*exp(-k_vec(lnum(:))*dx(:))
    end subroutine wavefunc
    
    
    !DEC$ ATTRIBUTES FORCEINLINE :: Transfer_Matrix
    function Transfer_Matrix(h,k,k_vec) result (tmijk) !i=1..2, j=1..2, k=1..N-1
    integer(c_int),intent(in)           ::  h
    integer,intent(in)                  ::  k
    complex(pc),intent(in)              ::  k_vec(:)
    complex(pc)                     ::  tmijk(2,2), kd, kd_res, ratio_pl, ratio_mn
    
    associate(mass=> strs(h)%mass, D=> strs(h)%D, U=> strs(h)%U)
        if(k_vec(k+1).EQ.(0_pr,0_pr))then
            tmijk(1,1)= exp(k_vec(k)*D(k))*mass(k+1)/mass(k)*k_vec(k)
            tmijk(1,2)= -exp(-k_vec(k)*D(k))*mass(k+1)/mass(k)*k_vec(k)
            tmijk(2,1)= exp(k_vec(k)*D(k))
            tmijk(2,2)= exp(-k_vec(k)*D(k))
            return
        end if
        if(k_vec(k).EQ.(0_pr,0_pr))then
            tmijk(1,1)= D(k)/2_pr+ mass(k+1)/mass(k)/2_pr /k_vec(k+1)
            tmijk(1,2)= (0.5_pr,0_pr)
            tmijk(2,1)= D(k)/2_pr- mass(k+1)/mass(k)/2_pr /k_vec(k+1)
            tmijk(2,2)= (0.5_pr,0_pr)
            return
        end if
        kd= exp(k_vec(k)*D(k))
        kd_res= exp(-k_vec(k)*D(k))
        ratio_pl= (0.5_pr,0_pr)+mass(k+1)/mass(k)/2_pr*k_vec(k)/k_vec(k+1)
        ratio_mn= (0.5_pr,0_pr)-mass(k+1)/mass(k)/2_pr*k_vec(k)/k_vec(k+1)
        tmijk(1,1)= kd*ratio_pl
        tmijk(1,2)= kd_res*ratio_mn
        tmijk(2,1)= kd*ratio_mn
        tmijk(2,2)= kd_res*ratio_pl
    end associate
    end function Transfer_Matrix
    
    
    !DEC$ ATTRIBUTES FORCEINLINE :: Transfer_Matrix_ivch
    function Transfer_Matrix_ivch(h,k,k_vec) result (tmijk) !i=1..2, j=1..2, k=1..N-1
    integer(c_int),intent(in)           ::  h
    integer,intent(in)                  ::  k
    complex(pc),intent(in)              ::  k_vec(:)
    complex(pc)                     ::  tmijk(2,2), kd, ratio
    
    associate(mass=> strs(h)%mass, D=> strs(h)%D, U=> strs(h)%U)
        kd= k_vec(k)*D(k)
        ratio= mass(1)/mass(k)*k_vec(k)/k_vec(1)
        tmijk(1,1)= cos(kd)
        tmijk(1,2)= sin(kd)/ratio
        tmijk(2,1)= -ratio*sin(kd)
        tmijk(2,2)= cos(kd)
    end associate
    end function Transfer_Matrix_ivch

    end module libtm