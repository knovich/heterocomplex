#%%
import matplotlib.colors as clr
import matplotlib.pyplot as plt
import matplotlib as mpl
import bl
import structure as st
import calculation as ca
import pickle
import numpy as np
#%matplotlib qt

#%%
def prelim_experiment(experiment,job):
    result=[job(x) for x in experiment]
    for r,x in zip(result,experiment):
        bl.plot_wfs(x,bl.roots_from_contours(r))
    return result

def do_experiment(experiment,job,outfile):
    result=[job(x) for x in experiment]
    with open(outfile,'wb') as f:
        pickle.dump((experiment,result),f)
    return result

def exp_discr(field=0.01,num=100):
    period=[(0.5,2,1),(0.5,8,0),(0.5,2,1)]
    return [st.Structure(period,
                         field=field,
                         discr=d,
                         last=True,repeat=num)
            for d in range(10,110,10)]

def exp_field(start=0.002,end=0.02,
              num=10,rep=100,
              period=[(0.5,2,1),(0.5,8,0),(0.5,2,1)]):
    return [st.Structure(period,
                         field=fld,
                         discr=20,
                         last=True,
                         repeat=rep)
            for fld in np.linspace(start,end,num)]

def exp_num():
    period=[(0.5,2,1),(0.5,8,0),(0.5,2,1)]
    return [st.Structure(period,
                         field=0.01,
                         discr=60,
                         last=True,repeat=r)
            for r in range(90,101)]

def job_prelim(struct,num=2,im0=0.000000000005,im1=0.01):
    mid=ca.mid(struct.Uprofile[1].max(),struct.Uprofile[1][-1])
    dE=struct.period.len*struct.field
    print(struct.period,struct.field)
    c=ca.RectPath(mid-num*dE-im1*1j,mid+num*dE-im0*1j,epsilon=0.1,Nroots=2*num+1,aspect=2*(2*num+1))
    f=lambda x:np.angle(struct.eqn_stat(x))
    r,d=ca.all_roots(c,f,1e-15)
    d1=[]
    for x in d:
        x.epsilon=1
        a,b=ca.all_roots(x,f,1e-15)
        r+=a
        d1+=b
    return r+d1

def job_main(struct):
    dE=struct.period.len*struct.field
    print(struct.period,struct.field)
    f=lambda x:np.angle(struct.eqn_stat(x))
    result=[]
    bnds=np.linspace(struct.Uprofile[1][-1],
                     struct.Uprofile[1].max(),
                     struct.repeat/10)
    for i in range(len(bnds)-1):
        print('    ',i)
        c=ca.RectPath(bnds[i]-0.001j,
                      bnds[i+1]-0.000000000005j,
                      epsilon=0.1,
                      Nroots=10,
                      aspect=10)
        r,d=ca.all_roots(c,f,1e-15)
        d1=[]
        for x in d:
            x.epsilon=1
            a,b=ca.all_roots(x,f,1e-15)
            r+=a
            d1+=b
        result+=r
        result+=d1
    return result

def sort_series(s):
    key=lambda x:(-x.imag).max()
    return sorted(s,key=key)

Wtype=np.dtype([('delta',np.longcomplex),
                ('W',np.longdouble)])

def W_from_series(s,struct,jump=1):
    return np.array([(s[i+jump]-s[i],bl.W_roots(s[i+jump],s[i],struct)) 
                    for i in range(len(s)-jump)],
                    dtype=Wtype)

def merge_two_series(s1,s2,dE):
    d1=(s2[0]-s1[0]).real/dE
    d2=(s1[-1]-s2[-1]).real/dE
    c=np.sort_complex(np.concatenate((s1,s2)))
    return c[abs(int(d1)):len(s1)+len(s2)-abs(int(d2))]

def close_to_int_or_zero(a,tol=0.1):
    i=round(a)
    if abs(a-i)<tol:
        return True
    else:
        return False

def W_from_ser_list(ser_list,struct):
    dE=struct.period.len*struct.field
    good=[x for x in ser_list if is_good_series(x,struct)]
    try:
        i=1
        while close_to_int_or_zero((good[i][0]-good[0][0])/dE):
            i+=1
        s=merge_two_series(good[0],good[i],dE)
        tr1=np.array([(s[i]-s[i-1],bl.W_roots(s[i],s[i-1],struct)) 
                    for i in range(1,len(s),2)],
                    dtype=Wtype)
        tr2=np.array([(s[i]-s[i-1],bl.W_roots(s[i],s[i-1],struct)) 
                    for i in range(2,len(s),2)],
                    dtype=Wtype)
        return sorted([tr1,tr2],key=lambda x:x['delta'][0].real)
    except:
        stub=np.ndarray((1,),dtype=Wtype)
        stub[:]=np.nan
        return [stub,stub]
    
def plot_merged_roots(ser_list,struct):
    dE=struct.period.len*struct.field
    good=[x for x in ser_list if is_good_series(x,struct)]
    i=1
    while close_to_int_or_zero((good[i][0]-good[0][0])/dE):
        i+=1
    s=merge_two_series(good[0],good[i],dE)
    bl.plot_wfs(struct,root_t_from_series(s))
    
def ext_W_row(ws,func=np.argmin):
    return ws[func(ws['W'])]

def plot_Ws(strs,series):
    plt.figure()
    plt.yscale('log')
    for i in (1,2,3):
        a=np.array([ext_W_row(W_from_series(se[0],st,jump=i)) 
                    for se,st in zip(series,strs)])
        plt.plot(a['delta']/i,a['W'],marker='o')
        #plt.scatter(a['delta']/i,a['W'])
    
def avg_err(ws):
    return (np.average(ws['delta']),ws['delta'].real.min(),ws['delta'].real.max(),
            np.average(ws['W']),ws['W'].min(),ws['W'].max())

w_err_type=np.dtype([('delta',np.longcomplex),
                     ('d_min',np.longdouble),('d_max',np.longdouble),
                     ('W',np.longdouble),
                     ('W_min',np.longdouble),('W_max',np.longdouble)])

def plot_Ws_err(strs,series):
    plt.figure()
    plt.yscale('log')
    for i in (1,2,3):
        a=np.array([avg_err(W_from_series(se[0],st,jump=i))
                    for se,st in zip(series,strs)],dtype=w_err_type)
        plt.errorbar(a['delta']/i,a['W'],
                    xerr=[(a['delta']-a['d_min'])/i,(a['d_max']-a['delta'])/i],
                    yerr=[a['W']-a['W_min'],a['W_max']-a['W']],
                    capsize=4)
        
def plot_Ws_err_cross(strs,series,fignum=None,err=True):
    if fignum:
        plt.figure(fignum)
    else:
        plt.figure()
    plt.yscale('log')
    ws=[W_from_ser_list(se,st) for se,st in zip(series,strs)]
    w1=np.array([avg_err(x[0]) for x in ws],dtype=w_err_type)
    w2=np.array([avg_err(x[1]) for x in ws],dtype=w_err_type)
    xf=np.array([x.period.len*x.field/2 for x in strs])
    mask=np.isnan(w1['W']) | np.isnan(w2['W'])
    w1,w2,xf=(t[~mask] for t in (w1,w2,xf))
    for a in (w1,w2):
        if err:
            plt.errorbar(xf,a['W'],
                        xerr=[(a['delta']-a['d_min']),(a['d_max']-a['delta'])],
                        yerr=[a['W']-a['W_min'],a['W_max']-a['W']],
                        capsize=4)
        else:
            plt.plot(xf,a['W'],marker='o')
            #plt.scatter(xf,a['W'])
    return plt.gcf().number

def root_t_from_series(s):
    if not isinstance(s,np.ndarray):
        se=np.sort_complex(np.concatenate(s))
    else:
        se=s
    a=np.ndarray(se.shape,dtype=bl.root_type)
    a['value']=se
    a['re_err']=a['im_err']=0
    return a

def add_W_est(fignum,zone_w,d):
    plt.figure(fignum)
    lims=np.linspace(*plt.xlim(),100)
    c=3e18
    h=6.58e-16
    Wf=lambda x:10.0/137.0/16/3.141526*(zone_w*d/c/h)**2*x/h
    plt.plot(lims,np.array([Wf(x) for x in lims]),'--')

def is_proper_wf(r,struct):
    pts,wf=struct.wavefunc(r,1000)
    max_wf=pts[np.argmax(abs(wf))]
    if r.real>struct.period._struct['U'].max()-struct.field*max_wf:
        return False
    else:
        return True

def is_good_series(s,struct):
    dE=struct.period.len*struct.field
    defracs=((s-np.roll(s,1))[1:].real)/dE
    if np.all([bl.close_to_int(x)==1 for x in defracs]):
        if is_proper_wf(s[0],struct):
            return True
    return False
    
def select_good_series(ser_list,struct):
    return [x for x in ser_list if is_good_series(x,struct)]

#%%
ed=exp_discr()
r_discr=prelim_experiment(ed,job_prelim)

#%%
plt.figure()
tmp=[]
for i in range(9):
    a=bl.roots_from_contours(r_discr[i])
    b=bl.roots_from_contours(r_discr[i+1])
    tmp.append(a[0]['value']-b[0]['value'])
print(tmp)
plt.plot(range(9),tmp)

#%%
ed2=exp_discr()
r_discr2=prelim_experiment(ed2,job_prelim)

#%%
plt.figure()
tmp=[]
for i in range(9):
    a=bl.roots_from_contours(r_discr2[i])
    b=bl.roots_from_contours(r_discr2[i+1])
    tmp.append(a[0]['value']-b[0]['value'])
print(tmp)
plt.plot(range(9),tmp)

#%%
ed3=exp_discr()
r_discr3=prelim_experiment(ed3[1:2],job_prelim)

#%%
big_experiment=exp_field()
big_res_test=prelim_experiment(big_experiment,lambda x:job_prelim(x,num=5))

#%%
e_f_part=exp_field(start=0.004,end=0.008)
r_f_part=prelim_experiment(e_f_part,lambda x:job_prelim(x,num=5))

#%%
e_n=exp_num()
enum_res=prelim_experiment(e_n,lambda x:job_prelim(x,num=5))

#%%
for r,e in zip(big_res_test,big_experiment):
    print(e.field,' ',len(r))
    for y in bl.roots_from_contours(r):
        print(y['value'])
    print('=============')

#%%
p1=[(0.5,2,1),(0.5,8,0),(0.5,2,1)]
s1=[st.Structure(p1,field=0.006,discr=20,last=True,repeat=200)]
prelim_experiment(s1,lambda x:job_prelim(x,num=5))
s2=[st.Structure(p1,field=0.006,discr=20,last=True,repeat=50)]
prelim_experiment(s2,lambda x:job_prelim(x,num=5))

#%%
p1=[(0.5,2,1),(0.5,8,0),(0.5,2,1)]
s1=[st.Structure(p1,field=0.02,discr=20,last=True,repeat=100)]
r1=prelim_experiment(s1,lambda x:job_prelim(x,num=2))
s=sorted_series(bl.extract_series(bl.roots_from_contours(r1[0])['value'],
                                  s1[0]))

#%%
x=bl.bloch_zones(s1[0].period)
x[0][1]-x[0][0]

#%%
s1=[st.Structure(p1,field=0.004,discr=20,last=True,repeat=50)]
r1=prelim_experiment(s1,lambda x:job_prelim(x,num=5,im0=1e-15,im1=0.001))
rl1=bl.roots_from_contours(r1[0])
for i in range(len(rl1)-2):
    print(bl.W_roots(rl1[i+2]['value'],rl1[i]['value'],s1[0]))

#%%
rl2=bl.roots_from_contours(r2[0])
dw2=np.array([((rl2[i+1]['value']-rl2[i]['value']).real,
               bl.W_roots(rl2[i+1]['value'],
                          rl2[i]['value'],s2[0]))
              for i in range(len(rl2)-1)])
